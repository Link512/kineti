import paho.mqtt.client as mqtt
import os
import asyncio
import sys
from urllib.parse import urlparse
from dotenv import load_dotenv

load_dotenv()

max_messages = 15
if len(sys.argv) == 2:
    max_messages = int(sys.argv[1])

url_str = os.environ.get("MQTT_URL", "mqtt://localhost:1883")
url = urlparse(url_str)
topic = os.environ.get("MQTT_TOPIC", "test")
ca_file = os.environ.get("CA_FILE")
user = os.environ.get("MQTT_USER")
password = os.environ.get("MQTT_PASSWORD")


mqttc = mqtt.Client()
mqttc.tls_set(ca_certs=ca_file)

mqttc.connect(url.hostname, url.port)
mqttc.username_pw_set(user, password)

mqttc.loop_start()


async def send_msg(client):
    count = 1
    while True:
        client.publish(topic, f"#{count}", 1)
        print(f"Seding {count}")
        count += 1
        if count > max_messages:
            break
        await asyncio.sleep(1)


asyncio.run(send_msg(mqttc))

print("Stopping")
mqttc.loop_stop()
