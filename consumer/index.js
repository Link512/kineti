import mqtt from 'mqtt';
import fs from 'fs';

import { config } from 'dotenv';

config();

let url = 'mqtt://localhost:1883';
let topic = 'test';
let ca = '';
let username = '';
let password = '';

if (typeof process !== 'undefined') {
    url = process.env.MQTT_URL;
    topic = process.env.MQTT_TOPIC;
    username = process.env.MQTT_USER;
    password = process.env.MQTT_PASSWORD;
    ca = fs.readFileSync(process.env.CA_FILE);
}

const opts = {
    ca,
    username,
    password,
    protocol: 'mqtts',
};
const client = mqtt.connect(url, opts);

client.subscribe(topic);
client.on('message', (topic, message) => {
    console.log(`got message "${message}" from topic: ${topic}`);
});
