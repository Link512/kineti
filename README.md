# MQTT experimenting

## Producer

### Usage

```bash
cd producer
pip install -r requirements.txt
python main.py N
```

The producer will generate N messages and then exit. Leave empty for default (15)

## Consumer

### Usage

```bash
cd consumer
yarn install
node_modules/.bin/npx babel-node index.js
```
